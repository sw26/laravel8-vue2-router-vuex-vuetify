<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test-tab-mkb', 'App\Http\Controllers\Api\TestTabMkbController@showTabMkb');
// Route::get('test-tab-mkb?page={page}&per_page={per_page}&sort={sort}&desc={desc}', 'App\Http\Controllers\Api\TestTabMkbController@showTabMkb')
// Route::get('test-tab-mkb/page={page}/per_page={per_page}/sort={sort}/desc={desc}', 'App\Http\Controllers\Api\TestTabMkbController@showTabMkb')
//     ->where(['page' => '[0-9]+', 'per_page' => '[0-9]+', 'sort' => '[0-9]+', 'desc' => '[0-9]+']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
