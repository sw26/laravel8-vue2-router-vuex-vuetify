<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('test-tab-mkb2/page-{page}/per_page-{per_page}/sort-{sort}', 'App\Http\Controllers\Api\TestTabMkbController@showTabMkb')
Route::get('test-tab-mkb2/page-{page}/per_page-{per_page}', 'App\Http\Controllers\Api\TestTabMkbController@showTabMkb')
    ->where(['page' => '[0-9]+', 'per_page' => '[0-9]+']);
Route::get('/{any}', function () {
    return view('main');
})->where('any', '.*');

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
