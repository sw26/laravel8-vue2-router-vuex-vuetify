<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel 8 | Vue 2</title>
    </head>
    <body>
        <div id="app"></div>    

        {{-- в файле webpack.mix.js нужно добавить к mix.js(...) - .version(), потом необходимо сделать npm run dev заново, после этого появится измененная ссылка с версионированием ():  --}}
        {{-- <script src="/js/app.js?id=0483c4f07d840ba1a502"></script> --}}
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>