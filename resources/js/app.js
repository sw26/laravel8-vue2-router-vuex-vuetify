import Vue from 'vue'
import router from './router'
import App from './App.vue'
import vuetify from './plugins/vuetify';
// import '../../node_modules/vuetify/dist/vuetify.min.css'
import store from "./store";
// import Vuetify from 'vuetify/lib';

Vue.config.productionTip = false


new Vue({
    el: '#app',
    // VueRouter,
    store,
    vuetify,
    router,
    render: h => h(App),
})