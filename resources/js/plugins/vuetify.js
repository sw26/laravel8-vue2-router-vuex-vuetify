import Vue from 'vue';
import Vuetify from 'vuetify/lib'; // ../../../../node_modules/
import colors from 'vuetify/lib/util/colors'
// import '../../../node_modules/vuetify/dist/vuetify.min.css'
// import Vuetify from '../../../node_modules/vuetify/lib/framework'; // ../../../../node_modules/
// import Vuetify from '../../../node_modules/vuetify/dist/vuetify.min.css'; // ../../../../node_modules/

const opts = {
    // theme: {
    //     themes: {
    //         light: {
    //             primary: colors.purple,
    //             secondary: colors.grey.darken1,
    //             accent: colors.shades.black,
    //             error: colors.red.accent3,
    //         },
    //         dark: {
    //             primary: colors.blue.lighten3,
    //         },
    //     },
    // },
    // icons: {
    //     // https://vuetifyjs.com/en/features/icon-fonts/#usage
    //     iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    // },
}

Vue.use(Vuetify);


export default new Vuetify(opts)

// export default new Vuetify({
// });
