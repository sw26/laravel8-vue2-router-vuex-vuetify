// npm i vue-router
// потом создать данный файл 
import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/home';

// Зарегистрировать роутер как плагин
Vue.use(Router);

export default new Router({
    // history - чтоб были обычные слеши в путях
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/grid',
            name: 'grid',
            // lazy loading
            component: () => import('./pages/grid')
        }
    ]
});