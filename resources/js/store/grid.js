
import axios from "../config/axios";

export default {
    // в состоянии храним какие-то данные
    state: {
        dataListGrid: []
    },

    // мутации - это просто функции, которые как-то должны изменять состояние. они всегда синхронные
    mutations: {
        setDataListGrid(state, data) {
            state.dataListGrid = {
                data: data.data,
                keys: data.keys
            }
        }
    },

    // аналог computed свойств, функции, которые возвращают результат каких-то вычислений
    getters: {
        getDataListGrid(state) {
            // тут можно как-то фильтровать массивы (filter), изменять (map) и т.д.
            return state.dataListGrid;
        }
    },
    
    // асинхронные функции, которые работают с каким-то внешним апи
    actions: {
        async fetchDataListGrid(context, url) {
            await axios
                .get(url)
                .then(response => context.commit("setDataListGrid", response.data))
        }
    },
}