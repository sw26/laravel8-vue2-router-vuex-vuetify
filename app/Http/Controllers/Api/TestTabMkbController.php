<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\TestTabMkbRequest;
use App\Models\TabMkb;
use Illuminate\Http\Request;

// use Illuminate\Http\Request;

class TestTabMkbController extends BaseController
{



    // public function showTabMkb(Request $request)
    public function showTabMkb(TestTabMkbRequest $request)
    {

        // return $this->sendResponse([
        //     'rrr' => '111'
        // ]);

        // dd($request->page, $request->per_page);
        // dd($request);
        // dd($request->page);
        // dd($request->per_page);
        // dd($request->per_page);
        // dd($request->per_page);
        
        $sort = $request->sort ?: 'mkb_code';
        $desc = $request->desc === 'true' ? 'desc' : 'asc';

        $offset = ($request->page - 1) * $request->per_page;
        // $offset = $request->page * $request->per_page;
        // $tab_mkb = TabMkb::select('mkb_code', 'mkb_klass', 'mkb_block', 'mkb_name', 'last_edit')->limit(11)->get();
        $tab_mkb_template = TabMkb::select('mkb_code', 'mkb_klass', 'mkb_block', 'mkb_name', 'last_edit');
        
        $page_count = (int) (ceil($tab_mkb_template->count() / $request->per_page));
        // $page_count = $tab_mkb_template->count();

        // dd($page_count);

        // dd((int) ceil($tab_mkb_template->count() / $request->per_page));
        $tab_mkb = $tab_mkb_template->orderBy($sort, $desc)->limit($request->per_page)->offset($offset)->get();
        
        // foreach ($tab_mkb as $key => $value) {
        //     dump($key, $value, "");
        // }
        // dd("");
        
        // foreach ($tab_mkb->toArray()['data'] as $key => $value) {
        //     dump($key, $value, "");
        // }
        // dd("");
        
        // dd($tab_mkb->count());
        // dd($tab_mkb->toArray()['data']);
        // dd($tab_mkb);
        // $tab_mkb_array = $tab_mkb->toArray()['data'];
        // dd(json_encode($tab_mkb_array));
        
        $unprepared_keys = array_keys($tab_mkb->toArray()[0]);
        // dd($unprepared_keys);
        $headers = [];
        foreach ($unprepared_keys as $key => $value) {
            $temp_key = [
                'text' => \Str::ucfirst(str_replace('_', ' ', $value)),
                'value' => $value,
            ];
            $headers[] = $temp_key;
        }

        // dd($headers);
    
        sleep(1);

        return $this->sendResponse([
            'headers' => json_encode($headers),
            'data_list' => $tab_mkb->toJson(),
            'amount_of_elements' => $tab_mkb->count(),
            // 'total_records' => $tab_mkb_template->count(),
            'page_count' => $page_count,
            'sort_by' => $sort,
            'sort_desc' => $desc,
            // 'request' => $request,
        ]);
    
    }




    public function showTabMkb_old()
    {
        
        // $tab_mkb = TabMkb::select('mkb_code', 'mkb_klass', 'mkb_block', 'mkb_name', 'last_edit')->first();
        // $tab_mkb = TabMkb::select('mkb_code', 'mkb_klass', 'mkb_block', 'mkb_name', 'last_edit')->limit(1)->get();
        $tab_mkb = TabMkb::select('mkb_code', 'mkb_klass', 'mkb_block', 'mkb_name', 'last_edit')->limit(11)->get();

        
        $unprepared_keys = array_keys($tab_mkb->toArray()[0]);
        $headers = [];
        foreach ($unprepared_keys as $key => $value) {
            $temp_key = [
                'text' => \Str::ucfirst(str_replace('_', ' ', $value)),
                'value' => $value,
            ];
            $headers[] = $temp_key;
        }
    
        sleep(1);

        return $this->sendResponse([
            'headers' => json_encode($headers),
            'data_list' => $tab_mkb->toJson(),
            'amount_of_elements' => $tab_mkb->count(),
        ]);
    
    }

}