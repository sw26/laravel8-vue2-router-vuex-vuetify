<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TabMkb extends Model
{

    protected $table = 'tab_mkb';
    protected $primaryKey = 'mkb_code';
    public $incrementing = false;


    /**
     * Аксессор для поля tab_mkb.mkb_code, т.к. без него возвращает mkb_code с пробелами справа
     *
     * @param [type] $value
     * @return void
     */
    public function getMkbCodeAttribute($value)
    {
    
        return trim($value);
    
    }
}
